<?PHP
	class PostModel {
		public $postId;
		public $postOwner;
		public $postUserId;
		public $postTitle;
		public $postBody;
		public $postDateOfCreation;

		function __construct($resultSet) {
			if (isset($resultSet["post_id"])) {
				$this->postId = $resultSet["post_id"];
			}
			if (isset($resultSet["post_owner"])) {
				$this->postOwner = $resultSet["post_owner"];
			}
			if (isset($resultSet["post_user_id"])) {
				$this->postUserId = $resultSet["post_user_id"];
			}
			if (isset($resultSet["post_title"])) {
				$this->postTitle = $resultSet["post_title"];
			}
			if (isset($resultSet["post_body"])) {
				$this->postBody = $resultSet["post_body"];
			}
			if (isset($resultSet["post_dateOfCreation"])) {
				$this->postDateOfCreation = $resultSet["post_dateOfCreation"];
			}
		}
	}
?>