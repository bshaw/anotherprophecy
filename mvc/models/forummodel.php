<?PHP 
	class ForumModel {
		public $forumId;
		public $forumTitle;
		public $forumDescription;
		public $forumDateOfCreation;

		function __construct($resultSet) {
			if (isset($resultSet["forum_id"])) {
				$this->forumId = $resultSet["forum_id"];
			}
			if (isset($resultSet["forum_title"])) {
				$this->forumTitle = $resultSet["forum_title"];
			}
			if (isset($resultSet["forum_description"])) {
				$this->forumDescription = $resultSet["forum_description"];
			}
			if (isset($resultSet["forum_dateOfCreation"])) {
				$this->forumDateOfCreation = $resultSet["forum_dateOfCreation"];
			}
		}
	}

?>