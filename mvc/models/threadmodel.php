<?PHP
	class ThreadModel {
		public $threadId;
		public $threadOwner;
		public $threadUserId;
		public $threadTitle;
		public $threadBody;
		public $threadDateOfCreation;

		function __construct($resultSet) {
			if (isset($resultSet["thread_id"])) {
				$this->threadId = $resultSet["thread_id"];
			}
			if (isset($resultSet["thread_owner"])) {
				$this->threadOwner = $resultSet["thread_owner"];
			}
			if (isset($resultSet["thread_user_id"])) {
				$this->threadUserId = $resultSet["thread_user_id"];
			}
			if (isset($resultSet["thread_title"])) {
				$this->threadTitle = $resultSet["thread_title"];
			}
			if (isset($resultSet["thread_body"])) {
				$this->threadBody = $resultSet["thread_body"];
			}
			if (isset($resultSet["thread_dateOfCreation"])) {
				$this->threadDateOfCreation = $resultSet["thread_dateOfCreation"];
			}
		}
	}
?>