<?PHP 
	
	//require_once ("../_private/config.php");
	//require_once ("../_private/dbcontext.php");
	require_once ("config.php");
	require_once ("dbcontext.php");

	//echo Config::GetFontFolder() . 'TIMES.TTF';
	require_once ("errorhandler.php");
	require_once ("controller.php");
	require_once ("viewmodel.php");

	require_once ("controllers/defaultcontroller.php");
	require_once ("controllers/errorcontroller.php");
	require_once ("controllers/forumcontroller.php");

	require_once ("dtos/greetdto.php");
	require_once ("dtos/errordto.php");
	require_once ("dtos/forumdto.php");
	require_once ("dtos/editordto.php");

	require_once ("services/errorservice.php");
	require_once ("services/sqliteservice.php");
	require_once ("services/httpservice.php");
	require_once ("services/captchaservice.php");
	
	require_once ("models/forummodel.php");
	require_once ("models/threadmodel.php");
	require_once ("models/postmodel.php");
	
	class Route {

		/*
			php types from https://www.php.net/manual/en/function.gettype.php

			"boolean"
			"integer"
			"double" (for historical reasons "double" is returned in case of a float, and not simply "float")
			"string"
			"array"
			"object"
			"resource"
			"resource (closed)" as of PHP 7.2.0
			"NULL"
			"unknown type"

		*/

		public $defaultControllerClassname = 'DefaultController';
		public $defaultControllerAction = 'index';
		
		/* prettify the URL by routing /default/greet/Bob into query strings 
			json 
			{
				controller string:
					ControllerClassname:
			}
		*/
		public $routes = [	// controller
							"default" => [
								// controller class name
								"DefaultController" => [
									// controller action and method name
									"index" => [],
									"greet" => [  
										// $_GET map for prettifying URLs in URL order of appearance
										"username" 
									],
									"get_server_stats" => [ "key", "type" ],
									"captcha" => []
								]
							],
							"forum" => [
								"ForumController" => [
									"index" => [],
									"post" => [ "id" ],
									"posts" => [ "id" ],
									"threads" => [ "id" ],
									"editor" => [],
									"seed" => [],
									"create" => [ "id" ]
								]
							],
							"error" => [
								"ErrorController" => [
									"index" => [ "code" ],
									"error" => []
								]
							]
						];

		private $controller = '';
		private $action = '';

		function __construct() {

			new ErrorHandler($this);
			new Config();

			$this->action = $this->defaultControllerAction;

			$uri = preg_split("/[?#&\/]/", $_SERVER['REQUEST_URI']);

			// catch cases where the url is just example.com and direct to the default controller
			if (strlen($uri[1]) == 0) {
				$this->controller = $this->defaultControllerClassname;
			}
			foreach ($uri as $queryString) {
				$query = preg_split("/=/", $queryString);
				// the uri is in the form of /controller/action/id
				if (strlen($queryString) > 0 && !isset($query[1])) {

					// controller should always come before the action
					if (!isset($_GET['controller'])) {
						$_GET['controller'] = $query[0];
						$this->controller = $this->getControllerClassname($_GET['controller']);
					}
					else if (!isset($_GET['action'])) {
						$_GET['action'] = $query[0];
						$this->action = $query[0];
					}

					// get controller routing to set GET variables or whatever
					else {
						$this->controller = $this->getControllerClassname( $_GET['controller'] );
						$controllerActionArray = $this->getControllerActionArray( $_GET['controller'], $_GET['action'] );
					
						foreach ($controllerActionArray as $action ) {
							if (!isset($_GET[$action])) {
								$_GET[$action] = $query[0];
								break;
							}
						}
					}
				}

				// the uri is in the form of a regular query string and doesn't need formatting I guess?
				else {
					if (isset($_GET['controller'])) {
						$this->controller = $this->getControllerClassname($_GET['controller']);
					}
					if (isset($_GET['action'])) {
						$this->action = $_GET['action'];
					}
				}
			}
		}
		private function getControllerActionArray($controller, $action) {
			
			if (isset($this->routes[ $controller ][$this->getControllerClassname($controller)][$action])) {
				return $this->routes[ $controller ][$this->getControllerClassname($controller)][$action];
			}
			return [];
		}
		private function getControllerClassname($controller) {
			if (isset($this->routes[ $controller ] )) {
				$controllerClassArray = $this->routes[ $controller ];

				if(isset(array_keys($controllerClassArray)[0])) {
					return array_keys($controllerClassArray)[0];
				}
			}
			return $this->controller;
		}

		public function getController() {
			$code = 404;
			if (class_exists($this->controller)) {
				$viewController = new $this->controller();
				
				if (method_exists($viewController, $this->action)) {

					// set the controllers view
					$viewController->setView( 
						call_user_func_array( array($viewController, $this->action), [] )
					);
					return $viewController;
				}
				else {
					$code = 400;
				}
			}

			$errorController = new ErrorController();
			$_GET["code"] = $code;
			$errorController->setView($errorController->index());
			return $errorController;
		}
	}

?>