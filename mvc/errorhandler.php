<?PHP 
	class ErrorHandler {

		function __construct($router) {
			
			function OnErrorHandler($errno, $errstr, $errfile, $errline) {
				error_log("{\"type\": \"OnError\",\"level\": \"$errno\",\"message\": \"$errstr\",\"file\": \"$errfile\",\"line\": \"$errline\"}");
				echo "<p><h3>An error has occurred on this page</h3>This error has been logged.</p>";
			}

			function OnExceptionHandler($exception) {
				error_log("{\"type\": \"OnException\", \"exception\": \"$exception\"}");
				echo "<p><h3>An exception has occurred on this page</h3>This exception has been logged.</p>";
			}

			ini_set("display_startup_errors", 0);
			ini_set("display_errors", 0);
			ini_set("html_errors", 0);
			ini_set("log_errors", 1);
			ini_set("error_reporting", 32767);
			ini_set("error_log", Config::$projectFolder . "/php-errors.log");

			set_error_handler( "OnErrorHandler" );
			set_exception_handler( "OnExceptionHandler" );

		}

		function __destruct() {

		}
	}

?>