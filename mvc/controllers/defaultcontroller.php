<?PHP 

	class DefaultController extends Controller {
		//private $view = NULL;
		function __construct() {

		}
		/*function __construct($route) {
			$this->dbContext = (new DBContext())->getContext();

			switch($route->getAction()) {
				case "greet": {
					$username = ( isset($_GET['user']) ? $_GET['user'] : "" );
					$this->view = $this->greet($username);
					break;
				}
				case "stats": {
					$key = ( isset($_GET['key']) ? $_GET['key'] : "" );
					$type = ( isset($_GET['type']) ? $_GET['type'] : "" );
					$this->view = $this->get_server_stats($key, $type);
					break;
				}
				case "captcha": {
					$this->view = $this->captcha();
					break;
				}
				default: {
					$this->view = $this->index();
					break;
				}
			}
		}*/
		function __destruct() {
			$this->dbContext = NULL;
		}
		/*public function getViewModel() {
			return $this->viewModel;
		}
		public function getView() {
			include  (Config::GetProjectRoot() . $this->view);
		}*/
		public function index() {
			return "views/index.php";
		}
		public function greet() {
			$username = '';
			if (isset($_GET['username'])) {
				$username = $_GET['username'];
			}
			$this->viewModel = new GreetDto($username);
			return "views/greet.php";
		}
		public function get_server_stats() {
			$key = '';
			$type ='';
			if (isset($_GET['key'])) {
				$key = $_GET['key'];
			}
			//echo $_GET['key'];
			if (isset($_GET['type'])) {
				$type = $_GET['type'];
			}
			$this->viewModel = [0 => $key, 1 => $type];
			return "views/stats.php";
		}
		public function captcha() {
			return (new Captcha())->generate();
			//return "views/captcha.php";
		}
	}
?>