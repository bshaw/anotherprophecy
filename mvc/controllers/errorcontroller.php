<?PHP 

	class ErrorController extends Controller {
		//private $dbContext = NULL;
		//private $viewModel = NULL;
		function __construct() {
			//$this->dbContext = (new DBContext())->getContext();
			//$this->view = $this->index($route->getAction());

		}
		function __destruct() {
			//$this->dbContext = NULL;
		}
		/*public function getViewModel() {
			return $this->viewModel;
		}
		public function getView() {
			return include ($this->view);
		}*/
		// todo: error handling is sort of all over the place
		public function index() {
			//$err = new ErrorService($code);

			//$dto = new ErrorDto($err->msg);

			//$this->viewModel = $dto;
			//echo $code;
			$code = 400;
			if (isset($_GET['code'])) {
				$code = $_GET['code'];
			}
			$err = new ErrorService($code);
			if (strcmp($code, "404") == 0) {
				return "views/404.html";
			}
			else {
				$this->viewModel = new ErrorDto($err->msg);
				return "views/error.php";
			}

		}
		public function error() {
			iAmError();
		}
	}
?>