<?PHP

	class ForumController extends Controller {
		//private $dbContext = NULL;
		//private $viewModel = NULL;

		//private $db;

		function __construct() {
			$this->viewModel = new ViewModel();
			if (session_status() !== PHP_SESSION_ACTIVE ) {
				session_start();
			}

			/*switch($route->getAction()) {
				case "create": {
					$this->view = $this->create();
					break;
				}
				case "seed": {
					$this->view = $this->seed();
					break;
				}
				case "editor": {
					$this->view = $this->editor();
					break;
				}
				case "threads": {
					$this->view = $this->threads();
					break;
				}
				case "posts": {
					$this->view = $this->posts();
					break;
				}
				case "post": {
					$this->view = $this->post();
					break;
				}
				default: {
					$this->view = $this->index();
					break;
				}
			}*/

		}
		function __destruct() {
			$this->dbContext = NULL;
		}
		/*public function getViewModel() {
			return $this->viewModel;
		}
		public function getView() {
			return include ($this->view);
		}*/
		public function post() {

			//$threadId = $id;//$_GET["id"];
			$threadId = 0;
			if (isset($_GET["id"])) {
				$threadId = $_GET["id"];
				//return "views/forum/posts.php";
			}

			if (!isset($_POST["post-create-hiddenform"]) || !isset($threadId) || !isset($_POST["post-create-title"]) || !isset($_POST["post-create-body"])) {
				return "views/forum/posts.php";
			}

			

			if (!Config::IsForumDisabled()) {

				$captcha = $_POST["post-create-hiddenform"];
				if ((new Captcha())->consume($captcha) == 0) {

					$userId = -1;
					if (isset($_SESSION["forum_user_id"])) {
						$userId = $_SESSION["forum_user_id"];
					}

					
					$title = htmlspecialchars($_POST["post-create-title"]);
					$body = htmlspecialchars($_POST["post-create-body"]);

					$db = new SQLiteService();
					$db->createPost($threadId, $userId, $title, $body);

				}

			}
			//HttpService::Redirect("?controller=forum&action=posts&id=$threadId");
			HttpService::Redirect("/forum/posts/$threadId");
			//return "views/forum/posts.php?id=$threadId";
		}

		public function posts() {
			$this->viewModel = new ForumDto();

			$threadId = 0;
			if (isset($_GET["id"])) {
				$threadId = $_GET["id"];
				//return "views/forum/posts.php";
			}

			$db = new SQLiteService();
			$this->viewModel->setPosts($db->getThreadPosts($threadId));
			return "views/forum/posts.php";
		}
		public function threads() {
			$this->viewModel = new ForumDto();

			//$forumId = $id;
			$id = 0;
			if (isset($_GET["id"])) {
				$id = $_GET["id"];
			}
			if ($id != 0) {
				$db = new SQLiteService();
				$this->viewModel->setThreads($db->getForumThreads($id));
				$this->viewModel->setActiveThreadId($id);
				return "views/forum/threads.php";
			}

			return "views/forum/index.php";
		}
		public function editor() {
			$this->viewModel = new EditorDto();


			if (!Config::IsForumDisabled()) {

				$this->viewModel->setResult("creating a new forum thing");

				if (isset($_POST["forum-create-hiddenform"]) && isset($_POST["forum-create-title"]) && isset($_POST["forum-create-body"])) {

					$captcha = $_POST["forum-create-hiddenform"];
					if ((new Captcha())->consume($captcha) == 0) {

						$title = htmlspecialchars($_POST["forum-create-title"]);
						$body = htmlspecialchars($_POST["forum-create-body"]);

						$db = new SQLiteService();
						$db->createForum($title, $body);

					}

					$this->viewModel->setResult("created a new forum");
				}
			}
			return "views/forum/editor.php";
		}
		public function seed() {
			$db = new SQLiteService();
			$db->seed();
			return "views/forum/seed.php";
		}
		public function create() {
			// todo: need to create each form with a unique session key to prevent spam
			$this->viewModel = new ForumDto();
			$db = new SQLiteService();


			if (!Config::IsForumDisabled()) {
				if (isset($_POST["thread-create-hiddenform"]) && isset($_POST["thread-forum-id"]) && isset($_POST["thread-create-title"]) && isset($_POST["thread-create-body"])) {

					$captcha = $_POST["thread-create-hiddenform"];
					if ((new Captcha())->consume($captcha) == 0) {
						$this->viewModel->setResult("new post created");

						$title = htmlspecialchars($_POST["thread-create-title"]);
						$body = htmlspecialchars($_POST["thread-create-body"]);
						$forumId = $_POST["thread-forum-id"];
						$userId = -1;

						if (isset($_SESSION["forum_user_id"])) {
							$userId = $_SESSION["forum_user_id"];
						}
						$db->createThread($forumId, $userId, $title, $body);
					}
				}
			}

			if (isset($_GET["id"])) {
				$this->viewModel->setActiveThreadId($_GET["id"]);
			}
			$this->viewModel->setForums($db->getForums());
			return "views/forum/create.php";
		}
		public function index() {
			$this->viewModel = new ForumDto();
			
			$db = new SQLiteService();
			$resultSet = $db->getForums();
			
			$this->viewModel->setForums($resultSet);
			return "views/forum/index.php";
		}
	}
?>