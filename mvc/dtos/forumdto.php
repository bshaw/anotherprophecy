<?PHP

	class ForumDto extends ViewModel {
		
		//public $result = NULL; // contains a message to relay transaction results to user
		public $forums = array();
		public $threads = array();
		public $posts = array();

		public $threadId = -1;

		
		function __construct() {

		}
		function __destruct() {
			
		}

		public function setThreads($resultSet) {
			foreach ($resultSet as $row) {
				$this->threads[] = new ThreadModel($row);
			}
		}
		public function setPosts($resultSet) {
			foreach ($resultSet as $row) {
				if (count($this->threads) == 0) {
					$this->threads[] = new ThreadModel($row);
				}
				$this->posts[] = new PostModel($row);
			}
		}
		public function setForums($resultSet) {
			foreach ($resultSet as $row) {
				$this->forums[] = new ForumModel($row);
			}
		}
		public function setActiveThreadId($id) {
			$this->threadId = $id;
		}
	}

?>