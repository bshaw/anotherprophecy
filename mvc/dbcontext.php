<?PHP 

	class DBContext {

		public static $port = 3306;
		public static $address = "localhost";
		public static $schema = "myschema";
		public static $username = "myuser";
		public static $password = "mypass";

		//private $t = Config::GetProjectRoot() . "/forumdb.db";
		//private  static $sql3Context = NULL;
		public static $sqlite3Database = __DIR__ . "/forumdb.db";

		private $dbContext = NULL;

		function __construct() {
			try {

				$this->dbContext = new PDO("mysql:host=".DBContext::$address.";port=".DBContext::$port.";dbname=".DBContext::$schema, DBContext::$username, DBContext::$password);
			}
			catch (PDOException $err) {
				//echo "PDOError" . $err;
			}
		}

		function __destruct() {
			$this->dbContext = NULL;
		}

		public function getContext() {
			return $this->dbContext;
		}
		static public function getContext3() {
			return DBContext::$sqlite3Database;
		}
	}
?>