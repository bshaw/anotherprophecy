<?PHP 
	$model = $this->getViewModel();
	$html = array();
?>
<script type="text/javascript" src="scripts/form.js"></script>

<span>hello forum world - editor</span>
<span><?PHP echo $model->result; ?></span>

<div style="border: 1px solid #fff; padding: 5px; margin: 0 10%;">
	<span>Create a new forum</span>
	<!--<form action="?controller=forum&action=editor" method="post" onsubmit="onFormSubmit(this, event)">-->
	<form action="/forum/editor" method="post" onsubmit="onFormSubmit(this, event)">
		<input class="hidden-field" type="text" name="forum-create-hiddenform" id="forum-create-hiddenform">
		<label for="forum-create-title">Forum Title</label><br/>
		<textarea type="text" name="forum-create-title" id="forum-create-title"></textarea>
		<br/>
		<label for="forum-create-body">Forum Description</label><br/>
		<textarea type="text" name="forum-create-body" id="forum-create-body"></textarea>
		<br/>
		<input type="submit">
	</form>
</div>

<br/>

<div style="border: 1px solid #fff; padding: 5px; margin: 0 10%;">
	<span>create and seed data</span>
	<br/>
	<!--<a href="?controller=forum&action=seed"><button>seed database</button></a>-->
	<a href="/forum/seed"><button>seed database</button></a>
</div>