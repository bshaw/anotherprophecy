<?PHP 
	$model = $this->getViewModel();

	$html = array();
	
	foreach($model->forums as $forum) {
		//$html[] = "<p><a href=\"?controller=forum&action=threads&id=$forum->forumId\">";
		$html[] = "<p><a href=\"/forum/threads/$forum->forumId\">";
		$html[] = "<div class=\"forum-group\">";
		$html[] = "<img class=\"forum-icon\" src=\"/favicon.ico\">";
		$html[] = "<dir class=\"forum-group-title\">$forum->forumTitle</dir>";
		$html[] = "<dir class=\"forum-group-description\">$forum->forumDescription</dir>";
		$html[] = "</div>";
		$html[] = "</a></p>";
	}

	$data = implode("", $html);
	if (strlen($data) > 0) {
		echo $data;
	}
	else {
		echo "There is nothing here";
	}
?>

<!--<a href="#">
	<div class="forum-group">
		<img class="forum-icon" src="favicon.ico">
		<dir class="forum-group-title">This is a forum test</dir>
		<dir class="forum-group-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dir>
	</div>
</a>
<a href="#">
	<div class="forum-group">
		<img class="forum-icon" src="favicon.ico">
		<dir class="forum-group-title">This is a forum test</dir>
		<dir class="forum-group-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</dir>
	</div>
</a>-->