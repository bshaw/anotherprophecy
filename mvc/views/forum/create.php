<?PHP 
	$model = $this->getViewModel();
	$html = array();
?>

<script type="text/javascript" src="/scripts/form.js"></script>

<span>hello forum world - create</span>

<!--<form action="?controller=forum&action=create" method="post" onsubmit="onFormSubmit(this, event)">-->
<form action=<?PHP echo "/forum/create/$model->threadId"; ?> method="post" onsubmit="onFormSubmit(this, event)">
	<input class="hidden-field" type="text" name="thread-create-hiddenform" id="thread-create-hiddenform">
	<label for="thread-forum-id">Forum</label><br/>
	<select name="thread-forum-id" id="thread-forum-id">
		<?PHP
			$html = array();
			foreach ($model->forums as $forum) {
				if ($forum->forumId == $model->threadId) {
					$html[] = "<option value=\"$forum->forumId\" selected>$forum->forumTitle</option>";
				}
				else {
					$html[] = "<option value=\"$forum->forumId\">$forum->forumTitle</option>";
				}
			}
			echo implode("", $html);
		?>
	</select>
	<br/>
	<label for="thread-create-title">Title</label><br/>
	<textarea type="text" name="thread-create-title" id="thread-create-title"></textarea>
	<br/>
	<label for="thread-create-body">Body</label><br/>
	<textarea type="text" name="thread-create-body" id="thread-create-body"></textarea>
	<br/>
	<input type="submit" value="Create Thread">
</form>
