<?PHP 

	class SQLiteService {
		private $dbContext = NULL;
		private $dbschema = NULL;
		function __construct() {
			$this->dbContext = new SQLite3(DBContext::getContext3(), SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

		}
		function __destruct() {
			$this->dbContext->close();
			$this->dbContext = NULL;
		}
		public function seed() {
			//return;

			// create tables
			$prep = $this->dbContext->prepare("PRAGMA foreign_keys = ON;");
			$prep->execute();

			// forums
			$prep = $this->dbContext->prepare("CREATE TABLE IF NOT EXISTS forums
				(
					forum_id INTEGER PRIMARY KEY,
					forum_title TEXT,
					forum_description TEXT
				);");
			$prep->execute();

			// threads
			$prep = $this->dbContext->prepare("CREATE TABLE IF NOT EXISTS threads
				(
					thread_id INTEGER PRIMARY KEY,
					thread_owner INTEGER,
					thread_user_id INTEGER,
					thread_title TEXT,
					thread_body TEXT,
					FOREIGN KEY (thread_owner) REFERENCES forums(forum_id)
				);");
			$prep->execute();

			// posts
			$prep = $this->dbContext->prepare("CREATE TABLE IF NOT EXISTS posts
				(
					post_id INTEGER PRIMARY KEY,
					post_owner INTEGER,
					post_user_id INTEGER,
					post_title TEXT,
					post_body TEXT,
					FOREIGN KEY (post_owner) REFERENCES threads(thread_id)
				);");
			$prep->execute();

			// altering tables
			$prep = $this->dbContext->prepare("ALTER TABLE forums ADD COLUMN forums_dateOfCreation DATE;");
			$prep->execute();

			$prep = $this->dbContext->prepare("ALTER TABLE threads ADD COLUMN threads_dateOfCreation DATE;");
			$prep->execute();

			$prep = $this->dbContext->prepare("ALTER TABLE posts ADD COLUMN post_dateOfCreation DATE;");
			$prep->execute();


		}
		public function createPost($threadId, $userId, $title, $body) {
			$prep = $this->dbContext->prepare("INSERT INTO posts (post_owner, post_user_id, post_title, post_body) 
				VALUES (:intPostOwner, :intPostUserId, :txtPostTitle, :txtPostBody);");
			$prep->bindParam(":intPostOwner", $threadId, SQLITE3_INTEGER);
			$prep->bindParam(":intPostUserId", $userId, SQLITE3_INTEGER);
			$prep->bindParam(":txtPostTitle", $title, SQLITE3_TEXT);
			$prep->bindParam(":txtPostBody", $body, SQLITE3_TEXT);
			$prep->execute();
		}

		public function createThread($forumId, $userId, $title, $body) {
			$prep = $this->dbContext->prepare("INSERT INTO threads (thread_owner, thread_user_id, thread_title, thread_body) 
				VALUES (:intThreadOwner, :intThreadUserId, :txtThreadTitle, :txtThreadBody);");
			$prep->bindParam(":intThreadOwner", $forumId, SQLITE3_INTEGER);
			$prep->bindParam(":intThreadUserId", $userId, SQLITE3_INTEGER);
			$prep->bindParam(":txtThreadTitle", $title, SQLITE3_TEXT);
			$prep->bindParam(":txtThreadBody", $body, SQLITE3_TEXT);
			$prep->execute();
		}


		public function createForum($title, $body) {
			$prep = $this->dbContext->prepare("INSERT INTO forums (forum_title, forum_description)
				VALUES (:txtForumTitle, :txtForumBody);");
			$prep->bindParam(":txtForumTitle", $title, SQLITE3_TEXT);
			$prep->bindParam(":txtForumBody", $body, SQLITE3_TEXT);
			$prep->execute();

		}

		public function getForums() {
			$prep = $this->dbContext->prepare("SELECT * FROM forums;");
			return $this->getResult($prep);
		}
		public function getForumThreads($forumId) {
			$prep = $this->dbContext->prepare("SELECT * FROM threads WHERE thread_owner = :intForumId;");
			$prep->bindParam(":intForumId", $forumId, SQLITE3_INTEGER);
			return $this->getResult($prep);
		}
		public function getThreadPosts($threadId) {
			$prep = $this->dbContext->prepare("SELECT * FROM threads AS t1 LEFT JOIN posts AS t2 
				ON t2.post_owner = t1.thread_id WHERE t1.thread_id = :intThreadId;");
			$prep->bindParam(":intThreadId", $threadId, SQLITE3_INTEGER);
			return $this->getResult($prep);
		}


		private function getResult($statement) {
			$result = array();
			$_set = $statement->execute();

			while (($set = $_set->fetchArray()) != FALSE) {
				$result[] = $set;
			}
			return $result;
		}

	}

?>