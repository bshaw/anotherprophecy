<?PHP 

	class ErrorService {
		public $msg = NULL;
		function __construct($code) {
			switch ($code) {
				case 500: {
					$this->msg = "<h2>500</h2> An Internal Server Error has occurred and been logged";
					header("HTTP/1.0 500 Internal Server Error");
					break;
				}
				case 403: {
					$this->msg = "<h2>403</h2> Forbidden";
					header("HTTP/1.0 403 Forbidden");
					break;
				}
				case 404: {
					$this->msg = "<h2>404</h2> Not Found";
					header("HTTP/1.0 404 Not Found");
					break;
				}
				case 400:
				default: {
					$this->msg = "<h2>400</h2> Bad Request";
					header("HTTP/1.0 400 Bad Request");
					break;
				}
			}
		}
	}

?>