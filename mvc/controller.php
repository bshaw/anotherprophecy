<?PHP 

	class Controller {
		protected $dbContext = NULL;
		protected $view = NULL;
		protected $viewModel = NULL;
		function __construct() {
			//$this->dbContext = (new DBContext())->getContext();
		}
		function __destruct() {
			$this->dbContext = NULL;
		}
		public function getViewModel() {
			return $this->viewModel;
		}
		/*public function getView() {
			//include Config::GetProjectRoot($this->view);
			return Config::GetProjectRoot($this->view);
		}*/
		public function getView() {
			include Config::GetProjectRoot($this->view);
			//return Config::GetProjectRoot($this->view);
		}
		public function setView($v) {
			$this->view = $v;
		}
	}
?>