function onFormSubmit(e, event) {
	event.preventDefault();
	showCaptcha(e);
}

function awaitSubmit(form, captcha) {
	form.children[0].value = captcha;
	form.submit();
}

function showCaptcha(e) {
	var html = document.createElement("div");
	html.setAttribute("class", "modal");
	html.onclick = function(e) {
		if (e.target.className === "modal") {
			html.remove();
		}
	}

	var container = document.createElement("div");
	container.setAttribute("class", "modal-contents");

	var img = document.createElement("img");
	img.src = "/?controller=default&action=captcha";

	var input = document.createElement("input");
	input.placeholder = "_ _ _";
	input.onkeydown = function(e) {
		if (e.keyCode == 13) {
			button.click();
		}
	}

	var button = document.createElement("button");
	button.innerText = "Continue";
	button.onclick = function() {
		awaitSubmit(e, input.value);
		//document.body.remove(html);
		html.remove();
	}

	var label = document.createElement("label");
	label.innerText = "Enter the number:";

	container.appendChild(img);
	container.appendChild(label);
	container.appendChild(input);
	container.appendChild(button);

	html.appendChild(container);

	document.body.appendChild(html);
	input.focus();
}