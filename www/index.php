<?PHP

	//ob_start();
	require ("../_private/mvc/route.php");
	//require ("../mvc/route.php");

	$route = new Route();
	$controller = $route->getController();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Some nerdy projects are found here.">
		<meta name="keywords" content="blog, dev, development, game, games, gaming, geek, nerd, programs, programming, software, tools">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=0, minimal-ui">
		<link type="text/css" rel="stylesheet" href="/style.css">
		<title>
			Hello
		</title>
	</head>
	<body>

	<div id="content">
		<p></p>
		<?PHP $controller->getView(); ?>
		<p></p>
	</div>

	<!-- footer -->
	<div style="position: fixed; left: 0; bottom: 0; font-size: 0.5em;">AnotherProphecy.com and its contents are copyright &copy; 2010 - <?PHP echo date("Y"); ?></div>

	</body>
</html>