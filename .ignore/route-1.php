<?PHP 
	
	//require_once ("../_private/config.php");
	//require_once ("../_private/dbcontext.php");
	require_once ("config.php");
	require_once ("dbcontext.php");

	//echo Config::GetFontFolder() . 'TIMES.TTF';

	require ("controller.php");
	require ("viewmodel.php");

	require ("controllers/defaultcontroller.php");
	require ("controllers/errorcontroller.php");
	require ("controllers/forumcontroller.php");

	require ("dtos/greetdto.php");
	require ("dtos/errordto.php");
	require ("dtos/forumdto.php");
	require ("dtos/editordto.php");

	require ("services/errorservice.php");
	require ("services/sqliteservice.php");
	require ("services/httpservice.php");
	require ("services/captchaservice.php");
	
	require ("models/forummodel.php");
	require ("models/threadmodel.php");
	require ("models/postmodel.php");

	function func($test1 = 0, $test2 = 1) {
		echo $test1 . "<br/>" . $test2 . "<br/>";
	}
	
	class Route {
		

		
		private $tokens = array();
		private $controller = 'default';
		private $action = 'index';

		private $queryStrings = array();

		/*
			php types from https://www.php.net/manual/en/function.gettype.php

			"boolean"
			"integer"
			"double" (for historical reasons "double" is returned in case of a float, and not simply "float")
			"string"
			"array"
			"object"
			"resource"
			"resource (closed)" as of PHP 7.2.0
			"NULL"
			"unknown type"

		*/

		private $routes = 	[
								"default" => [
									"DefaultController" => [
										"index" => [],
										"greet" => [  "username" ],
										"get_server_stats" => [ "key", "type" ],
										"captcha" => []
									]
								],
								"forum" => [
									"ForumController" => [
										"index" => [],
										"post" => [ "id" ],
										"posts" => [ "id" ],
										"threads" => [ "id" ],
										"editor" => [],
										"seed" => [],
										"create" => []
									]
								],
								"error" => [
									"ErrorController" => [
										"index" => [ "int" ]
									]
								]
							];

		function __construct() {

			/* todo: 
			instead of query strings https://anotherprophecy.com/?controller=mycontroller&action=myaction&randomarg1=more&randomarg2=things
			routing needs to become https://anotherprophecy.com/mycontroller/myaction/more/things

			so relying on this hardcoded method is probably a big no no*/

			

			$uri = preg_split("/[?#&\/]/", $_SERVER['REQUEST_URI']);

			// check if route is serving just the index
			if (count($uri) > 1) {
				//print_r(explode("&=", $_SERVER["REQUEST_URI"]));

				$uriCount = 0;
				// if uri is ?controller=control&action=do&id=1 attempt to set controller and action and keep a record query strings
				foreach ($uri as $values) {
					if (strlen($values) > 0) {
						$query = preg_split("/=/", $values);
						
						if (strcmp(strtolower($query[0]), "controller") == 0) {
							$this->controller = $query[0];
						}

						else if (strcmp(strtolower($query[0]), "action") == 0) {
							$this->controller = $query[0];
						}

						else {

							// set controller and action to whatever the 1st and 2nd 
							if ($uriCount == 0) {
								$this->controller = $query[0];
							}
							else if ($uriCount == 1) {
								$this->action = $query[0];
							}

							// neither controller or action but a query string that should be passed along to the view
							else if (isset($query[1])) {
								// query string is in the form of &id=1
								$this->queryStrings[$uriCount-2] = $query[1];//[$query[0]] = $query[1];
							}

							// query string is in the form of ../../1
							else {
								$this->queryStrings[$uriCount-2] = $query[0];
							}
							$uriCount++;
						}

					}

				}

			}

			if (isset($_POST['controller'])) {
				$this->controller = strtolower($_POST['controller']);
			}
			if (isset($_GET['controller'])) {
				$this->controller = strtolower($_GET['controller']);
			}
			if (isset($_POST['action'])) {
				$this->action = strtolower($_POST['action']);
			}
			if (isset($_GET['action'])) {
				$this->action = strtolower($_GET['action']);
			}

		}
		function __destruct() {
			//$this->dbContext = NULL;
		}
		public function getAction() {
			return $this->action;
		}

		public function getController() {
			$errCode = 400;
			// get controller class
			
			// check if controller portion of route is known
			if (isset($this->routes[$this->controller])) {
				$cc = $this->routes[$this->controller];
				$controllerClass = array_keys($cc)[0];


				// check if action portion of route is known
				if (isset($cc[$controllerClass][$this->action])) {
					$controllerView = $this->action;

					// redundant check to see if controller classes exist
					if (class_exists($controllerClass)) {

						$viewController = new $controllerClass();
						
						// another redudancy to check if controller method exists
						if (method_exists($viewController, $this->action)) {
							$args = array();
							// check if the the number of user query strings matches the view argument count
							//if (count($this->queryStrings) < count($cc[$controllerClass][$this->action])) {
								
								// set default arguments so php doesnt freak the fuck out
								$counter = 0;
								//print_r($cc[$controllerClass][$this->action]);

								foreach($cc[$controllerClass][$this->action] as $primitive) {

									// set arg to whatever querystring is in the order that the user or view has them set
									if (isset($this->queryStrings[$counter])) {
										$args[] = $this->queryStrings[$counter];
									}

									// there was no querystring set so associate primitive (and string) types with a default value
									else {
										//$args[] = NULL;
										// match 
										//echo $primitive;
										switch (strtolower($primitive)) {
											
											case "int": {
												$args[] = 0;
												break;
											}

											// php uses double in place of float/real
											case "double": {
												$args[] = 0.0;
												break;
											}

											case "string": {
												$args[] = "";
												break;
											}

											default: {
												die("something horrible has happened and millions of electrons may have lost their lives");
											}

										}
										//$args[] = 
									}
									$counter++;
								}
							//}
							// get view expected arguments
							foreach($cc[$controllerClass][$this->action] as $butts) {
								//$args
							}

							/*$args = array();
							foreach($this->queryStrings as $value) {
								//$args[] = $
							}*/
							// set view
							/*print_r($cc[$controllerClass][$this->action]);
							echo "<br/>";
							print_r($args);
							echo "<br/>";
							print_r($this->queryStrings);
							echo "<br/>";*/

							$viewController->setView(call_user_func_array(array($viewController, $this->action), $args));//$this->queryStrings));

							return $viewController;

						}

					}

				}

			}

			//echo "<br/>";
			// check if controller route exists
			/*if (isset($this->routes[$this->controller])) {
				$_controller = $this->routes[$this->controller];
				// try to get controller class name
				if (isset(array_keys(array($this->routes[$this->controller])[0])[0])) {
					$controllerClass = array_keys(array($this->routes[$this->controller])[0])[0];
					// get view arguments
					$args = array();
					if (isset($this->routes[$this->controller][$controllerClass][$this->action])) {
						foreach ($this->routes[$this->controller][$controllerClass][$this->action] as $action) {
							if (isset($_GET[$action])) {
								$args[$action] = $_GET[$action];
							}
							else if (isset($_POST[$action])) {
								$args[$action] = $_POST[$action];
							}
							else {
								$args[$action] = "";
							}
						}
					}
					// if class exists create new instance and attempt to call the view
					if (class_exists($controllerClass)) {
						$controllerAction = $this->action;
						$viewController = new $controllerClass();
						if (method_exists($viewController, $controllerAction)) {
							$viewController->setView(call_user_func_array(array($viewController, $controllerAction), $args));
							return $viewController;
						}
					}
					$errCode = 404;
				}
			}*/

			// error handling
			$default = new ErrorController();
			$default->setView($default->index($errCode));
			return $default;
		}
	}

?>