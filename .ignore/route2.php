<?PHP 
	
	//require_once ("../_private/config.php");
	//require_once ("../_private/dbcontext.php");
	require_once ("config.php");
	require_once ("dbcontext.php");

	require ("controller.php");
	require ("viewmodel.php");

	require ("controllers/defaultcontroller.php");
	require ("controllers/errorcontroller.php");
	require ("controllers/forumcontroller.php");

	require ("dtos/greetdto.php");
	require ("dtos/errordto.php");
	require ("dtos/forumdto.php");
	require ("dtos/editordto.php");

	require ("services/errorservice.php");
	require ("services/sqliteservice.php");
	require ("services/httpservice.php");
	require ("services/captchaservice.php");
	
	require ("models/forummodel.php");
	require ("models/threadmodel.php");
	require ("models/postmodel.php");
	
	class Route {
		/* todo: 
		instead of query strings https://anotherprophecy.com/?controller=mycontroller&action=myaction&randomarg1=more&randomarg2=things
		routing needs to become https://anotherprophecy.com/mycontroller/myaction/more/things

		so relying on this hardcoded method is probably a big no no
		*/
		/*private $action = NULL;
		private $controller = NULL;

		public $clientIP = NULL;
		public $username = NULL;*/


		function __construct() {
			/*$uri = preg_split("/[?#&\/]/", $_SERVER['REQUEST_URI']);

			$uri_tokens = array();
			$uri_length = count($uri);

			if ($uri_length >= 3) {
				$uri_tokens = [	"controller" => $uri[1], 
								"action" => $uri[2]];
			}
			else {
				$uri_tokens["controller"] = "default";
				$uri_tokens["action"] = "index";
			}

			for($i1 = 3; $i1 < $uri_length; $i1++) {
				$uri_tokens["query".($i1-2)] = $uri[$i1];
			}
			$this->tokens = $uri_tokens;*/
			$this->tokens["controller"] = "default";
			$this->tokens["action"] = "index";

			// fallback gets
			if (isset($_GET['controller'])) {
				//$this->controller = strtolower($_GET['controller']);
				$this->tokens["controller"] = strtolower($_GET['controller']);
			}
			else {
				//$this->controller = strtolower($uri_sorted[""]);
			}
			if (isset($_GET['action'])) {
				//$this->action = strtolower($_GET['action']);
				$this->tokens["action"] = strtolower($_GET['action']);
			}
			//print_r($this->tokens);
		}
		function __destruct() {
			//$this->dbContext = NULL;
		}
		public function getAction() {
			return $this->tokens["action"];
		}
		public function getController() {
			switch ($this->tokens["controller"]) {
				case "error": {
					return new ErrorController($this);
				}
				case "forum": {
					return new ForumController($this);
				}
				case "default": 
				default: {
					return new DefaultController($this);
				}
			}
		}

	}
?>